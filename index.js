global.confPath = process.argv.length > 2 ? process.argv[2] : __dirname + "/config.json";

async function createHTTPServer() {
    const express = require('express');
    const app = express();
    const bodyParser = require('body-parser');
    const http = require('http');
	await require('./services/MongoDB').init();
    await require('./services/Email').init();

    await require("./services/CronLider").init();
    await require("./services/CronFalabella").init();
    await require("./services/CronParis").init();
    await require("./services/CronMercadoLibre").init();
    await require("./services/CronHites").init();

    //await require("./services/CronRipley").init();
    //await require("./services/CronAdidas").init();

    app.use(bodyParser.text({ type: 'text/*' }));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json({ limit: "50mb" }));
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, puresocial");
        next();
    });

    var port = 8111;
    httpServer = http.createServer(app);
    httpServer.listen(port, function() {
        console.log("[Ratazo] HTTP Server iniciado en puerto " + port);
    });
};

createHTTPServer();