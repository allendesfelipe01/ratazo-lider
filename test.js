

function test () {
    let text= `<div class="h-100" data-pid="914625001">
    <div class="product-tile h-100 js-product-tile-container " data-gtm="{&quot;event&quot;:&quot;ImpressionsUpdate&quot;,&quot;eventTypes&quot;:&quot;show&quot;,&quot;ecommerce&quot;:{&quot;currencyCode&quot;:&quot;CLP&quot;,&quot;impressions&quot;:{&quot;category&quot;:&quot;notebook&quot;,&quot;name&quot;:&quot;Notebook 14 Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD&quot;,&quot;price&quot;:419990,&quot;id&quot;:&quot;914625001&quot;}}}" data-url="/on/demandware.store/Sites-HITES-Site/default/Product-Variation?pid=914625001" data-pid="914625001">
    
    <div class="product-tile-image-container">
    <div class="image-container">
    <a class="image-item js-tile-image-container" href="/notebook-14-lenovo-ideapad-flex-5-amd-ryzen-3-8-gb-ram-integrated-amd-radeon-graphics-256-gb-ssd-914625001.html">
    <img class="img-fluid w-100 tile-image js-image1" src="https://www.hites.com/dw/image/v2/BDPN_PRD/on/demandware.static/-/Sites-mastercatalog_HITES/default/dw13636b86/images/original/pim/914625001/914625001_5.jpg?sw=303&amp;sh=303" alt="Notebook 14&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD" title="Notebook 14&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD" onerror="window.replaceImage(this)" />
    <img class="img-fluid w-100 tile-image js-image2 d-none" src="https://www.hites.com/dw/image/v2/BDPN_PRD/on/demandware.static/-/Sites-mastercatalog_HITES/default/dw9be490fa/images/original/pim/914625001/914625001_1.jpg?sw=303&amp;sh=303" alt="Notebook 14&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD" title="Notebook 14&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD" onerror="window.replaceImage(this)" />
    </a>
    </div>
    <div class="product-tile-badges">
    <span class="discount-badge bl-badge">-11%</span>
    <span class="image-badge tl-badge">
    <img class="img-fluid w-100" src="/on/demandware.static/-/Sites/default/dwf0cbbfd1/images/ribbons/ribbon-retiro-gratis.svg" alt="Product badge">
    </span>
    <span class="image-badge tr-badge">
    <img class="img-fluid w-100" src="/on/demandware.static/-/Sites-mastercatalog_HITES/default/dw35e7c005/images/badges/touch.svg" alt="Product badge">
    </span>
    <span class="image-badge br-badge">
    <img class="img-fluid w-100" src="/on/demandware.static/-/Sites/default/dw035db68a/images/ribbons/ribbon-despacho24horas-rm-v-vi.svg" alt="Product badge">
    </span>
    </div>
    </div>
    <div class="product-tile-body">
    <div class="tile-section">
    <div class="tile-half description-section">
    <span class="d-block product-brand">LENOVO</span>
    <a class="link product-name--bundle" href="/notebook-14-lenovo-ideapad-flex-5-amd-ryzen-3-8-gb-ram-integrated-amd-radeon-graphics-256-gb-ssd-914625001.html" data-gtm="{&quot;event&quot;:&quot;ProductName&quot;,&quot;eventTypes&quot;:&quot;click,hover&quot;,&quot;action&quot;:&quot;Notebook 14 Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD&quot;,&quot;label&quot;:&quot;/notebook-14-lenovo-ideapad-flex-5-amd-ryzen-3-8-gb-ram-integrated-amd-radeon-graphics-256-gb-ssd-914625001.html&quot;}">Notebook 14&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD</a>
    <span class="d-block product-sku">
    SKU: 914625001
    </span>
    <div class="product-marketplace">
    <span class="marketplace-info-plp">Por: <b>Hites</b></span>
    </div>
    <input type="hidden" value="Notebook 14&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD" name="product-name" />
    <input type="hidden" value="LENOVO" name="product-brand" />
    </div>
    <div class="tile-half price-section pr-1">
    <div class="oos-tags d-none">
    <div class="price tag">
    <div class="product-tags">
    <span class="tag-item outofstock">&iexcl;SIN STOCK!</span>
    </div>
    </div>
    </div>
    <div class="availability-secondary-message">
    </div>
    <div class="">
    <div class="prices-section">
    <div class="price">
    <span class="price-item hites-price">
    $399.990
    <svg class="icon">
    <use xlink:href="#tarjeta-hites-icon"></use>
    </svg>
    </span>
    <span class="price-item sales strike-through">
    <span class="value" content="419990">
    $419.990
    Oferta
    </span>
    </span>
    <span class="price-item list strike-through ">
    <span class="value" content="449990">
    <span class="sr-only">
    Price reduced from
    </span>
    $449.990
    Normal
    <span class="sr-only">
    to
    </span>
    </span>
    </span>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="tile-section list-bottom-section">
    <div class="stars-section-grid top-stars">
    <span class="yotpo-stars personalizado">
    
    </span>
    </div>
    <div class="tile-half variations-buy-section">
    <div class="variations-section">
    </div>
    <div class="btn-buy-section ">
    <input type="hidden" class="add-to-cart-url" value="/on/demandware.store/Sites-HITES-Site/default/Cart-AddProduct" />
    <input type="hidden" class="variation-url" value="https://www.hites.com/on/demandware.store/Sites-HITES-Site/default/Product-Variation" />
    <button class="hsg-btn hsg-btn-buy buy-btn" type="button" data-master-pid="914625001" data-pid="914625001" data-ga4="{&quot;event_ga4&quot;:&quot;add_to_cart&quot;,&quot;currency&quot;:&quot;CLP&quot;,&quot;value&quot;:419990,&quot;page_type&quot;:&quot;plp&quot;,&quot;button_type&quot;:&quot;add_basket&quot;,&quot;item&quot;:{&quot;item_id&quot;:&quot;914625001&quot;,&quot;item_name&quot;:&quot;Notebook 14\&quot; Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD&quot;,&quot;item_brand&quot;:&quot;Lenovo&quot;,&quot;item_category&quot;:&quot;Notebook&quot;,&quot;currency&quot;:&quot;CLP&quot;,&quot;item_category2&quot;:&quot;Imperdibles Cyber&quot;,&quot;item_category3&quot;:&quot;Especial Tarjeta Hites&quot;,&quot;item_list_name&quot;:&quot;Notebook&quot;,&quot;item_list_id&quot;:&quot;notebook&quot;,&quot;hites_price&quot;:399990,&quot;discount&quot;:399990,&quot;price&quot;:419990,&quot;item_variant&quot;:&quot;914625001&quot;,&quot;promotion_id&quot;:&quot;DGRATISPUP-02052023&quot;,&quot;promotion_name&quot;:&quot;DG PUP Starken por compras sobre $19.990&quot;,&quot;marketplace&quot;:&quot; &quot;}}">
    Comprar
    <svg class="icon"><use xlink:href="#right-circle-arrow"></use></svg>
    </button>
    <div class="d-none">
    </div>
    <a class="link goto-product" href="/notebook-14-lenovo-ideapad-flex-5-amd-ryzen-3-8-gb-ram-integrated-amd-radeon-graphics-256-gb-ssd-914625001.html" data-gtm="{&quot;event&quot;:&quot;ProductName&quot;,&quot;eventTypes&quot;:&quot;click,hover&quot;,&quot;action&quot;:&quot;Notebook 14 Lenovo Ideapad Flex 5 / AMD Ryzen 3 / 8 GB RAM / Integrated AMD Radeon Graphics / 256 GB SSD&quot;,&quot;label&quot;:&quot;/notebook-14-lenovo-ideapad-flex-5-amd-ryzen-3-8-gb-ram-integrated-amd-radeon-graphics-256-gb-ssd-914625001.html&quot;}">Ver producto</a>
    </div>
    </div>
    <div class="d-none options-div">
    </div>
    <div class="tile-half list-bottom-left">
    <div class="attributes-section">
    <ul class="list-unstyled main-attributes">
    <li class="attribute-values">
    Almacenamiento:
    256 GB
    </li>
    <li class="attribute-values">
    Memoria RAM:
    8 GB
    </li>
    <li class="attribute-values">
    Procesador:
    Amd Ryzen 3
    </li>
    </ul>
    </div>
    <div class="yotpo-stars-and-shipping-section">
    <div class="stars-section-list bottom-stars">
    <span class="yotpo-stars personalizado">
    </span>
    </div>
    <div class="shipping-compare-section">
    <div class="shipping-section">
    <div class="pdp-shipping-method-shipping shipping-method " data-target="#shippingModal">
    <svg class="icon" data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;span class=&quot;font-weight-bold  available&quot;&gt;Disponible"><use xlink:href="#shipping-icon"></use></svg>
    <div class="method-description">
    <span class="d-block">Despacho a Domicilio</span>
    <span class="font-weight-bold  available">Disponible
    </div>
    </div>
    <div class="pdp-shipping-method-pickup shipping-method " data-target="#pickUpModal">
    <svg class="icon" data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;span class=&quot;font-weight-bold available&quot;&gt;Disponible"><use xlink:href="#pickup-icon"></use></svg>
    <div class="method-description">
    <span class="d-block">Puntos de Retiro</span>
    <span class="font-weight-bold available">Disponible
    </div>
    </div>
    <div class="pdp-shipping-method-sameDay shipping-method disabled">
    <svg class="icon" data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;span class=&quot;font-weight-bold unabailable&quot;&gt;No Disponible"><use xlink:href="#shipping-icon-express"></use></svg>
    <div class="method-description">
    <span class="d-block">Entrega mismo día</span>
    <span class="font-weight-bold unabailable">No Disponible
    </div>
    </div>
    </div>
    <div class="stars-section-grid bottom-stars">
    <span class="yotpo-stars personalizado">
    </span>
    </div>
    <div class="compare-section">
    <div class="compare">
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
    </div>
    </div>
    </div>`

    console.log(text.split(`<span class="value" content="`)[2].split(`">`)[0].trim());
};

test();