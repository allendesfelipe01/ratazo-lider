const timeZone = require("./Config").getConfig().timeZone;
const moment = require("moment-timezone");

class TimeUtils {
    static get now() {
        return moment.tz(timeZone)
    }
    static get moment() {
        return moment;
    }
    static fromUTCMillis(utcMillis) {
        return moment.tz(utcMillis, timeZone);
    }
}

module.exports = TimeUtils;