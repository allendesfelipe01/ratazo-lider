var axios = require('axios');
const mongoDB = require('./MongoDB');
const email = require('./Email');
const config = require("./Config").getConfig();
const tor_axios = require('tor-axios');

class CronParis {
	static get instance() {
		if (CronParis.singleton) return CronParis.singleton;
		CronParis.singleton = new CronParis();
		return CronParis.singleton;
	}

	constructor() {
		// Server Control
		this.heartbeartTimer = null;
		this.sleepTimer = 20 // segundos		
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;

		this.search_list = [];
		this.tor = null;
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list_paris');
		this.change_detection = await mongoDB.collection('change_detection_paris');
		this.products_change_detection = await mongoDB.collection('products_change_detection_paris');

		this.tor = tor_axios.torSetup({
			ip: 'localhost',
			port: 9050,
			controlPort: '9051',
			controlPassword: 'giraffe',
		});
		this.heartbeatExecute();
	};

	async heartbeatExecute() {
		const validateTimeOut = async () => {
			try {
				this.search_list = await this.search_list_collections.find({ active: true }).toArray();

				for (let i = 0; i < this.search_list.length; i++) {
					let f = this.search_list[i];
					let now = Date.now();
					let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
					let reduce = [];
					let products = [];
					let productosNuevos = [];
					let pages = f.pages || 1;

					for (let j = 1; j <= pages; j++) {
						let url = f.url;

						var config = {
							method: 'get',
							maxBodyLength: Infinity,
							url: f.url,
							headers: f.headers,
							timeout: 600000
						};
						// 0 24 48 72
						if(f.pages) url += `/0/${((j - 1) * 24)}/24?refine_1=cgid=${f.category}`
						config.url = url;

						console.log('[' + today + '][ paris - heartbeatExecute] init :', config.url);

						await this.tor.torNewSession()
						.then(async responseip => {				
							try {
								const inst = await axios.create({
									httpAgent: this.tor.httpAgent(),
									httpsagent: this.tor.httpsAgent(),
								});

								await inst(config)
									.then(async (response) => {
										products = response.data.payload.data.hits;

										for (let i = 0; i < products.length; i++) {
											let e = products[i];
											let clp_normal_price = e.prices['clp-list-prices'];
											let clp_internet_prices = null;
											let clp_list_prices = null;
											let clp_cencosud_prices = null;

											if(e.prices){
												clp_internet_prices = e.prices['clp-internet-prices'] || 9999999;
												clp_list_prices = e.prices['clp-list-prices'] || 9999999;
												clp_cencosud_prices = e.prices['clp-cencosud-prices'] || 9999999;
											}
											
											var min = Math.min(clp_normal_price, clp_internet_prices, clp_list_prices, clp_cencosud_prices);

											if ((100 - (( min /  clp_list_prices ) * 100)) >= f.discount) {
												reduce.push({
													sku: e.product_id,
													timestamp: now,
													date: today,
													displayName: e.product_name,
													url: "https://www.paris.cl/search?q=" + e.product_id,
													normalPrice: clp_normal_price != 9999999 ? new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(clp_normal_price) : 0,
													offerPrice: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(min),
													cardPrice: clp_cencosud_prices != 9999999 ? new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(clp_cencosud_prices): 0,
													saving: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(clp_normal_price - min),
													discount: Math.trunc((100 - ((min / clp_list_prices) * 100))),
													image: e.image ? e.image.link : null
												})
												if(!e.image){
													console.log(e);
												}
											} 
										};
									})
									.catch((error) => {
										console.log('[paris] axios error ', config.url, error);
									});


							} catch (error) {
								console.log('[paris] error global:', config.url, error);
							};
						});
					};

					for await (let r of reduce) {
						let found = await this.products_change_detection.findOne({ sku: r.sku });
						if (!found) {
							console.log("Product New :", r);
							productosNuevos.push(r);
							await this.products_change_detection.insertOne(r);
						} else if (found && (found.normalPrice != r.normalPrice || found.offerPrice != r.offerPrice || found.cardPrice != r.cardPrice)) {
							console.log("Product Update :", r);
							productosNuevos.push(r);
							await this.products_change_detection.updateOne({ sku: r.sku }, { $set: { normalPrice: r.normalPrice, offerPrice: r.offerPrice, cardPrice: r.cardPrice } });
						};
					};
					if (productosNuevos.length > 0) await this.sendNotification(productosNuevos);
				};
			} catch (e) {
				console.error(e);
			}finally {
				this.heartbeartTimer = setTimeout( validateTimeOut, 10000);
			};
		};
		validateTimeOut();
	};

	compareNumbers(a, b) {
		return a - b;
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms * 1000));
	}

	async stop() {
		if (this.heartbeartTimer) {
			console.log(`[CronParis][stop] > Eliminando Timer 'heartbeartTimer`);
			clearTimeout(this.heartbeartTimer);
			this.heartbeartTimer = null;
		}
	}

	async sendNotification(productosNuevos) {
		try {
			console.log("[CronParis] sendNotification", productosNuevos.length);

			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
						.pdp-mobile-sales-price-paris {
							font-size: 14px;
							font-weight: 500;
						}
						.pdp-mobile-sales-price {
							font-size: 14px;
							font-weight: 500;
							color: #414042;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
			productosNuevos.forEach(data => {
				html +=
					`<div class="row">		
						<div class="card">
							<div style="max-width: 20%" >
								<img src="${data.image}" alt="Image" style="max-width: 100px">
							</div>
							<div class="product-detail-card__product-prices" style="max-width: 80%">
								<div class="d-flex">
									<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 600;">${data.sku} </span> -  ${data.displayName}</a> </h1>
									<span class="pdp-mobile-sales-price-paris"> Tarjeta paris : ${data.cardPrice} </span>
								</div>

								<div class="d-flex">
									<span class="pdp-mobile-sales-price">Internet : ${data.offerPrice} </span>
									<span class="pdp-mobile-discount-percentage-card">-${data.discount}%</span>
								</div><div class="d-flex pdp-mobile-reference-price">
								<span class="saving__price__pdp"><del>${data.normalPrice}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
			});
			html += `</div> </body> </html>`;

			email.sendNotification(html, 'paris')
				.catch(error => {
					console.log('[Email][sendNotification] Send email error >', error);
					setTimeout(() => {
						email.sendNotification(html, 'paris');
					}, 5000);
				});
		} catch (err) {
			console.log('[CronParis][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = CronParis.instance;
