var axios = require('axios');
const mongoDB = require('./MongoDB');
const config = require("./Config").getConfig();
const tor_axios = require('tor-axios');
const email = require('./Email');

class CronHites {
	static get instance() {
		if (CronHites.singleton) return CronHites.singleton;
		CronHites.singleton = new CronHites();
		return CronHites.singleton;
	}

	constructor() {
		// Server Controlf
		this.heartbeartTimer = null;
		this.heartbeatInterval = 1; //minutos
		this.sleepTimer = 1 // segundos		
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;

		this.search_list = [];
		this.tor = null;
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list_hites');
		this.change_detection = await mongoDB.collection('change_detection_hites');
		this.products_change_detection = await mongoDB.collection('products_change_detection_hites');

		this.tor = tor_axios.torSetup({
			ip: 'localhost',
			port: 9050,
			controlPort: '9051',
			controlPassword: 'giraffe',
		});

		this.heartbeatExecute();
	};

	async heartbeatExecute() {
		const validateTimeOut = async () => {
			try {
				this.search_list = await this.search_list_collections.find({ active: true }).toArray();

				for (let i = 0; i < this.search_list.length; i++) {
					let f = this.search_list[i];
					let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
					let reduce = [];
					let productosNuevos = [];
					let pages = f.pages || 1;

					for (let j = 1; j <= pages; j++) {
						let url = f.url;

						var config = {
							method: 'get',
							url: f.url,
							maxBodyLength: Infinity,
							headers: f.headers,
							timeout: 5000
						};

						if (f.pages) url += `?sz=24&start=${24 * (j - 1)}&srule=best-matches`;
						config.url = url;

						console.log('[' + today + '][ hites - heartbeatExecute] init :', config.url);

						await this.tor.torNewSession()
							.then(async responseip => {
								try {
									const inst = await axios.create({
										httpAgent: this.tor.httpAgent(),
										httpsagent: this.tor.httpsAgent(),
									});

									await inst(config)
										.then(async (response) => {
											let response_hites = JSON.parse(response.data.split(`<script type="application/ld+json">`)[2].split('</script>')[0].trim());
											if (response_hites.itemListElement.length > 0) console.log('[hites] Cantidad de productos : ', response_hites.itemListElement.length);
											else console.log('No hay productos');

											productosNuevos = [];
											for (let i = 0; i < response_hites.itemListElement.length; i++) {
												let e = response_hites.itemListElement[i];
												let product = { ...e.item };

												if (!response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`]) || !response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1]) {
													console.log('Producto no tiene lo minimo para ser analizado, ' + e.item.sku);
													e.item.sku = e.item.sku + '001';
												};

												if (
													response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item sales ">`)
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item sales ">`)[1]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item sales ">`)[1].split(`<span class="sr-only">`)
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item sales ">`)[1].split(`<span class="sr-only">`)[0]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item sales ">`)[1].split(`<span class="sr-only">`)[1]
												) {
													let text = response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item sales ">`)[1].split(`<span class="sr-only">`);
													product.offer_price = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(text[0].concat(text[1]).split('content="')[1].split(`">\n`)[0]));
													product.normal_price = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(text[0].concat(text[1]).split('content="')[2].split(`">\n`)[0]));
													product.saving = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(text[0].concat(text[1]).split('content="')[2].split(`">\n`)[0]) - parseFloat(text[0].concat(text[1]).split('content="')[1].split(`">\n`)[0])),
														product.discount = - ((parseFloat(text[0].concat(text[1]).split('content="')[1].split(`">\n`)[0]) * 100 / parseFloat(text[0].concat(text[1]).split('content="')[2].split(`">\n`)[0])) - 100).toFixed(0);
												} else if (
													response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item hites-price">`)
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item hites-price">`)[1]
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item hites-price">`)[1].split(`<svg class="icon">`)
													&& response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item hites-price">`)[1].split(`<svg class="icon">`)[0]
												) {
													let hites_price = response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="price-item hites-price">`)[1].split(`<svg class="icon">`)[0].trim().replace(/\$/g, '').replace(/./g, '');
													let offer_price = response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="value" content="`)[1].split(`">`)[0].trim()
													let normal_price = response.data.split([`<div class="h-100" data-pid="${e.item.sku}">`])[1].split(`<div class="col-6 col-lg-4 px-0">`)[0].split(`<span class="value" content="`)[2].split(`">`)[0].trim()

													product.hites_price = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(hites_price));
													product.offer_price = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(offer_price));
													product.normal_price = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(normal_price));
													product.saving = new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseFloat(normal_price) - parseFloat(hites_price)),
														product.discount = - ((parseFloat(hites_price) * 100 / parseFloat(normal_price)) - 100).toFixed(0);
												} else {
													console.log('Producto no fue leido correctamente.');
												};
												if (parseInt(product.discount) >= f.discount) reduce.push(product);
											};
										})
										.catch((error) => {
											console.log('[hites] axios error ', config.url, error.message);
										});
								} catch (error) {
									console.log('[hites] error global:', config.url, error.message);
								};
							});
					};
					for await (let r of reduce) {
						let found = await this.products_change_detection.findOne({ sku: r.sku });
						if (!found) {
							productosNuevos.push(r);
							console.log("Product New :", r);
							await this.products_change_detection.insertOne(r);
						} else if (found && found.offer_price != r.offer_price) {
							productosNuevos.push(r);
							console.log("Product Update :", r);
							await this.products_change_detection.updateOne({ sku: r.sku }, { $set: { offer_price: r.offer_price, normal_price: r.normal_price } });
						};
					};
					if (productosNuevos.length > 0) await this.sendNotification(productosNuevos);
				}
			} catch (e) {
				console.error(e);
			}finally {
				this.heartbeartTimer = setTimeout( validateTimeOut, 5000);
			};
		};
		validateTimeOut();

	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms * 1000));
	}

	async stop() {
		if (this.heartbeartTimer) {
			console.log(`[CronHites][stop] > Eliminando Timer 'heartbeartTimer`);
			clearTimeout(this.heartbeartTimer);
			this.heartbeartTimer = null;
		}
	}

	async sendNotification(productosNuevos) {
		try {
			console.log("[CronHites] sendNotification", productosNuevos.length)

			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
						.pdp-mobile-sales-price-hites {
							font-size: 18px;
							font-weight: 500;
						}
						.pdp-mobile-sales-price {
							font-size: 18px;
							font-weight: 600;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
						img {
							max-height: 100px; /* Adjust the height value as per your requirements */
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
			productosNuevos.forEach(data => {
				html +=
					`<div class="row">		
						<div class="card">
							<div style="margin-right: 30px">
								<img src="${data.image}" alt="Image">
							</div>
							<div class="product-detail-card__product-prices">
								<div class="d-flex">
									<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 700;">${data.sku} </span> -  ${data.name}</a> </h1>
									<span class="pdp-mobile-sales-price">${data.offer_price}</span>
									<span class="pdp-mobile-discount-percentage-card"> - ${data.discount}%</span>
								</div>`
				if (data.hites_price) {
					html += `
									<div class="d-flex">
										<span class="pdp-mobile-sales-price-hites"> Tarjeta Hites : ${data.hites_price || 0}</span>
									</div>`
				}
				html +=
					`<div class="d-flex pdp-mobile-reference-price">
									<span class="saving__price__pdp"><del>${data.normal_price}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
			});
			html += `</div> </body> </html>`;

			email.sendNotification(html, 'hites')
				.catch(error => {
					console.log('[Email][sendNotification] Send email error >', error);
					setTimeout(() => {
						email.sendNotification(html, 'hites');
					}, 5000);
				});

		} catch (err) {
			console.log('[CronHites][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = CronHites.instance;
