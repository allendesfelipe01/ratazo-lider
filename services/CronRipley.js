var axios = require('axios');
const mongoDB = require('./MongoDB');
const email = require('./Email');
const config = require("./Config").getConfig();
const tor_axios = require('tor-axios');

class CronRipley {
	static get instance() {
		if (CronRipley.singleton) return CronRipley.singleton;
		CronRipley.singleton = new CronRipley();
		return CronRipley.singleton;
	}

	constructor() {
		// Server Control
		this.heartbeartTimer = null;
		this.heartbeatInterval = 3; //minutos
		this.sleepTimer = 2 // segundos		
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;

		this.search_list = [];
		this.tor = null;
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list_ripley');
		this.change_detection = await mongoDB.collection('change_detection_ripley');
		this.products_change_detection = await mongoDB.collection('products_change_detection_ripley');
		
		this.tor = tor_axios.torSetup({
			ip: 'localhost',
			port: 9050,
			controlPort: '9051',
			controlPassword: 'giraffe',
		});

		this.heartbeatExecute(true);
	};

	callHeartbeatExecute(first) {
		const validateTimeOut = async () => {
			try {
				this.search_list = await this.search_list_collections.find({ active: true }).toArray();
	
				for (let i = 0; i < this.search_list.length; i++) {
					let f = this.search_list[i];
					let now = Date.now();
					let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
					let reduce = [];
					let productosNuevos = [];
					let pages = f.pages || 1;
	
					for (let j = 1; j <= pages; j++) {
						let url = f.url;
	
						var config = {
							method: 'get',
							url: f.url,
							headers: f.headers,
							timeout: 5000
						};
					
						if(f.pages) url += `&page=${j}`;
						config.url = url;
	
						console.log('[' + today + '][ Ripley - heartbeatExecute] init :', config.url);
						
						await this.tor.torNewSession()
						.then( async responseip => {
							//change tor ip
							console.log(responseip);
							await this.sleep(this.sleepTimer);
							try{
	
								let response_newip = await this.tor.get('http://api.ipify.org');
								console.log('[ripley] ', response_newip.data);
	
								const inst = await axios.create({
									httpAgent: this.tor.httpAgent(),
									httpsagent: this.tor.httpsAgent(),
								});
		
								await inst(config)
									.then(async (response) => {
										let response_ripley = JSON.parse(response.data.split('window.__PRELOADED_STATE__ = ')[1].split('</script>')[0].slice(0,-4));
										if(response_ripley.products.length > 0) console.log('[Ripley] Cantidad de productos : ' , response_ripley.products.length);
										else console.log('No hay productos');
										productosNuevos = [];
										
										for (let i = 0; i < response_ripley.products.length; i++) {
											let e = response_ripley.products[i];
	
											if (e.prices && e.prices.discountPercentage >= f.discount && e.SKUs[0].stock) {
												
												let arrSort = [e.prices.listPrice, e.prices.offerPrice, e.prices.cardPrice ];
												arrSort.sort(this.compareNumbers);
	
												reduce.push({ 
													sku : e.SKUs[0].partNumber, 
													searchId: f._id.toString(), 
													timestamp: now, 
													date: today, 
													displayName: e.name, 
													url: e.url,
													listPrice: e.prices.formattedListPrice,
													offerPrice: e.prices.formattedOfferPrice,
													cardPrice: e.prices.formattedCardPrice,
													price : arrSort[0],
													saving : new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format( e.prices.discount ),   
													discount: e.prices.discountPercentage, 
													image: e.fullImage.slice(2)
												})
											}
										};
	
										if (reduce.length > 0) {
											let last_change_detection = await this.change_detection.find({ searchId: f._id.toString() }).sort({ timestamp: -1 }).limit(1).toArray();
											if (last_change_detection.length == 0) {
												let result = await this.change_detection.insertOne({
													timestamp: now,
													date: today,
													searchId: f._id.toString()
												});
												last_change_detection = result.ops[0];
											} else {
												last_change_detection = last_change_detection[0];
											}
			
											let products_last_change_detection = await this.products_change_detection.find(
												{
													changeDetectionId: last_change_detection._id.toString(),
													searchId: f._id.toString()
												}
											).toArray();
	
											if (products_last_change_detection.length == 0) {
												for (let i = 0; i < reduce.length; i++) {
													let r = reduce[i];
													await this.products_change_detection.insertOne({
														changeDetectionId: last_change_detection._id.toString(),
														...r
													});												
													productosNuevos.push(r);
												};
											} else {
												reduce.forEach(async r => {
													let found = false;
													for (let i = 0; i < products_last_change_detection.length; i++) {
														let p = products_last_change_detection[i];
														if (p.sku == r.sku && p.discount == r.discount && p.listPrice == r.listPrice && p.offerPrice == r.offerPrice && r.cardPrice == p.cardPrice  ) {
															found = true;
														};
													};
			
													if (!found) {													
														productosNuevos.push(r);
														await this.products_change_detection.insertOne({
															changeDetectionId: last_change_detection._id.toString(),
															...r
														});
													};
												});
											};
										}
									})
									.catch((error) => {
										console.log('[ripley] axios error ', config.url, error.response);
									});
							}catch(error){
								console.log('[ripley] error global:', config.url,  error);
							};
						});
					};


					if (productosNuevos.length > 0) await this.sendNotification(productosNuevos);
				}
			} catch (e) {
				console.error(e);
			} finally {
				this.heartbeartTimer = setTimeout( validateTimeOut, 5000);
			};
		};
		validateTimeOut();

	}

	async heartbeatExecute() {
		try {
			this.search_list = await this.search_list_collections.find({ active: true }).toArray();

			for (let i = 0; i < this.search_list.length; i++) {
				let f = this.search_list[i];
				let now = Date.now();
				let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
				let reduce = [];
				let productosNuevos = [];
				let pages = f.pages || 1;

				for (let j = 1; j <= pages; j++) {
					let url = f.url;

					var config = {
						method: 'get',
						url: f.url,
						headers: f.headers,
						timeout: 5000
					};
				
					if(f.pages) url += `&page=${j}&s=mdco`;
					config.url = url;

					console.log('[' + today + '][ Ripley - heartbeatExecute] init :', config.url);
					
					await this.tor.torNewSession()
					.then( async responseip => {
						//change tor ip
						console.log(responseip);
						await this.sleep(this.sleepTimer);
						try{

							let response_newip = await this.tor.get('http://api.ipify.org');
							console.log('[ripley] ', response_newip.data);

							const inst = await axios.create({
								httpAgent: this.tor.httpAgent(),
								httpsagent: this.tor.httpsAgent(),
							});
	
							await inst(config)
								.then(async (response) => {
									let response_ripley = JSON.parse(response.data.split('window.__PRELOADED_STATE__ = ')[1].split('</script>')[0].slice(0,-4));
									if(response_ripley.products.length > 0) console.log('[Ripley] Cantidad de productos : ' , response_ripley.products.length);
									else console.log('No hay productos');
									productosNuevos = [];
									
									for (let i = 0; i < response_ripley.products.length; i++) {
										let e = response_ripley.products[i];

										if (e.prices && e.prices.discountPercentage >= f.discount && e.SKUs[0].stock) {
											
											let arrSort = [e.prices.listPrice, e.prices.offerPrice, e.prices.cardPrice ];
											arrSort.sort(this.compareNumbers);

											reduce.push({ 
												sku : e.SKUs[0].partNumber, 
												searchId: f._id.toString(), 
												timestamp: now, 
												date: today, 
												displayName: e.name, 
												url: e.url,
												listPrice: e.prices.formattedListPrice,
												offerPrice: e.prices.formattedOfferPrice,
												cardPrice: e.prices.formattedCardPrice,
												price : arrSort[0],
												saving : new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format( e.prices.discount ),   
												discount: e.prices.discountPercentage, 
												image: e.fullImage.slice(2)
											})
										}
									};

									if (reduce.length > 0) {
										let last_change_detection = await this.change_detection.find({ searchId: f._id.toString() }).sort({ timestamp: -1 }).limit(1).toArray();
										if (last_change_detection.length == 0) {
											let result = await this.change_detection.insertOne({
												timestamp: now,
												date: today,
												searchId: f._id.toString()
											});
											last_change_detection = result.ops[0];
										} else {
											last_change_detection = last_change_detection[0];
										}
		
										let products_last_change_detection = await this.products_change_detection.find(
											{
												changeDetectionId: last_change_detection._id.toString(),
												searchId: f._id.toString()
											}
										).toArray();

										if (products_last_change_detection.length == 0) {
											for (let i = 0; i < reduce.length; i++) {
												let r = reduce[i];
												await this.products_change_detection.insertOne({
													changeDetectionId: last_change_detection._id.toString(),
													...r
												});												
												productosNuevos.push(r);
											};
										} else {
											reduce.forEach(async r => {
												let found = false;
												for (let i = 0; i < products_last_change_detection.length; i++) {
													let p = products_last_change_detection[i];
													if (p.sku == r.sku && p.discount == r.discount && p.listPrice == r.listPrice && p.offerPrice == r.offerPrice && r.cardPrice == p.cardPrice  ) {
														found = true;
													};
												};
		
												if (!found) {													
													productosNuevos.push(r);
													await this.products_change_detection.insertOne({
														changeDetectionId: last_change_detection._id.toString(),
														...r
													});
												};
											});
										};
									}
								})
								.catch((error) => {
									console.log('[ripley] axios error ', config.url, error.response);
								});
						}catch(error){
							console.log('[ripley] error global:', config.url,  error);
						};
					});
				};
				if (productosNuevos.length > 0) await this.sendNotification(productosNuevos);
			}
		} catch (e) {
			console.error(e);
			throw e;
		} finally {
			this.callHeartbeatExecute(false);
		}
	}

	compareNumbers(a, b) {
		return a - b;
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms*1000));
	}

	async stop() {
		if (this.heartbeartTimer) {
			console.log(`[CronRipley][stop] > Eliminando Timer 'heartbeartTimer`);
			clearTimeout(this.heartbeartTimer);
			this.heartbeartTimer = null;
		}
	}

	async sendNotification(productosNuevos) {
		try {
			console.log("[CronRipley] sendNotification", productosNuevos.length);

			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
						.pdp-mobile-sales-price-ripley {
							font-size: 14px;
							font-weight: 500;
						}
						.pdp-mobile-sales-price {
							font-size: 14px;
							font-weight: 500;
							color: #414042;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
				productosNuevos.forEach(data => {
					html += 	
					`<div class="row">		
						<div class="card">
							<div style="max-width: 20%" >
								<img src="${data.image}" alt="Image" style="max-width: 100px">
							</div>
							<div class="product-detail-card__product-prices" style="max-width: 80%">
								<div class="d-flex">
									<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 600;">${data.sku} </span> -  ${ data.displayName }</a> </h1>
									<span class="pdp-mobile-sales-price-ripley"> Tarjeta Ripley : ${data.cardPrice} </span>
								</div>

								<div class="d-flex">
									<span class="pdp-mobile-sales-price">Internet : ${data.offerPrice} </span>
									<span class="pdp-mobile-discount-percentage-card">-${data.discount}%</span>
								</div><div class="d-flex pdp-mobile-reference-price">
								<span class="saving__price__pdp"><del>${data.listPrice}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
				});
			html += `</div> </body> </html>`;

			email.sendNotification(html, 'ripley')
			.catch( error => {
				console.log('[Email][sendNotification] Send email error >', error);
				setTimeout( () => {
					email.sendNotification(html, 'ripley');
				}, 5000);
			});
		} catch (err) {
			console.log('[CronRipley][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = CronRipley.instance;
