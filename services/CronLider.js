var axios = require('axios');
const mongoDB = require('./MongoDB');
const config = require("./Config").getConfig();
const email = require('./Email');

class CronLider {
	static get instance() {
		if (CronLider.singleton) return CronLider.singleton;
		CronLider.singleton = new CronLider();
		return CronLider.singleton;
	}

	constructor() {
		// Server Control
		this.heartbeartTimer = null;
		this.sleepTimer = 1; // segundos
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;

		this.search_list = [];
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list');
		this.change_detection = await mongoDB.collection('change_detection');

		this.heartbeatExecute();	
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	async heartbeatExecute() {
		const validateTimeOut = async () => {
			try {
				this.search_list = await this.search_list_collections.find({ active: true }).toArray();
				for( let f of this.search_list){
					try{
						this.executeUrl(f)
					}catch(error){
						console.log("search_list", error)
					};
				};
			} catch (error_search_list) {
				console.error("error_search_list", error_search_list);
			} finally {
				this.heartbeartTimer = setTimeout( validateTimeOut, 30000);
			};
		};
		validateTimeOut();
	}

	async executeUrl(f){
		let now = Date.now();
		let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
		let reduce = [];
		
		let pages = f.pages || 1;
		console.log('[' + today + '][CronLider - heartbeatExecute] init :', f.category );
		for (let j = 1; j <= pages; j++) {
			let data_obj = {};
			if (f.type == 'category') {
				data_obj = {
					"categories": f.category,
					"page": j,
					"facets": [],
					"hitsPerPage": 100
				};
			} else {
				data_obj = {
					"page": j,
					"facets": [],
					"sortBy": "discount_desc",
					"hitsPerPage": 100,
					"query": f.query
				}
			}
			if (f.storeId) data_obj['storeId'] = f.storeId;

			var data = JSON.stringify(data_obj);

			var config = {
				method: 'post',
				url: f.url,
				maxBodyLength: Infinity,
				headers: {
					'Content-Type': 'application/json',
					'Cookie': 'TS018b674b=01c448d1670cdf77292989c715739a8b03a73db49a5939ff733cac5eeb8827364733d7693d0a615ecfaa7509d2cf1abe0b3aa5c2cc'
				},
				data: data
			};
			if (f.headers) config.headers = f.headers;
			await axios(config)
				.then(async (response) => {
					for (let i = 0; i < response.data.products.length; i++) {
						let e = response.data.products[i];
						if (e.discount >= f.discount && e.available) {
							reduce.push({ sku : e.sku, timestamp: now, date: today, displayName: e.displayName, url: ( f.app ? `https://www.lider.cl/supermercado/search?query=${e.sku}` : `https://www.lider.cl/catalogo/search?query=${e.sku}` ), price: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(e.price.BasePriceSales), BasePriceReference: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(e.price.BasePriceReference), saving : new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(e.price.BasePriceReference - e.price.BasePriceSales),   discount: e.discount, image: e.images.mediumImage })
						}
					};
				})
				.catch((error) => {
					if(error) {
						console.log(config.data);
						console.error(error.message);
					}
				});
		};
		let skusVistos = {};
		let productosNuevos = [];
		let arrayDeObjetosDistintos = [];

		arrayDeObjetosDistintos = reduce.filter(objeto => {
			if (!skusVistos[objeto.sku]) {
			  skusVistos[objeto.sku] = true;
			  return true;
			}
			return false;
		});

		for( let r of arrayDeObjetosDistintos ){
			try{
				let products_change_detection = await mongoDB.collection('products_change_detection');
				let found = await products_change_detection.findOne({ sku : r.sku});
				if(found && found.price != r.price) console.log( found.price, r.price, typeof found.price, typeof r.price);
				if(!found){
					console.log("Product New :", r);
					productosNuevos.push(r);
					await products_change_detection.insertOne(r);
				}else if(found && found.price != r.price){
					productosNuevos.push(r);
					console.log("Product Update :", r);
					console.log(f.category);
					await products_change_detection.findOneAndUpdate({ sku : r.sku },{ $set: { ...r } });
				};
			}catch(errorreduce){
				console.error('[lider] reduce ', errorreduce)
			};
		};
		if (productosNuevos.length > 0) this.sendNotification(productosNuevos);
	}

	async sendNotification(pn) {
		try {
			console.log("sendNotification", pn.length);

			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
			
						.pdp-mobile-sales-price {
							font-size: 26px;
							font-weight: 700;
							color: #414042;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
				pn.forEach(data => {
					html += 	
					`<div class="row">		
						<div class="card">
							<div style="width: 20%" >
								<img src="${data.image}" alt="Image">
							</div>
							<div class="product-detail-card__product-prices">
								<div class="d-flex">
								<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 700;">${data.sku} </span> -  ${ data.displayName }</a> </h1>
								<span class="pdp-mobile-sales-price">${data.price}</span>
								<span class="pdp-mobile-discount-percentage-card">${data.discount}%</span>
								</div><div class="d-flex pdp-mobile-reference-price">
								<span class="saving__price__pdp"><del>${data.BasePriceReference}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
				});
			html += `</div> </body> </html>`;
							
	
			email.sendNotification(html, 'lider')
			.catch( error => {
				console.log('[Email][sendNotification] Send email error >', error);
				setTimeout( () => {
					email.sendNotification(html, 'lider');
				}, 5000);
			});
			return;
		} catch (err) {
			console.log('[Billing][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = CronLider.instance;
