var axios = require('axios');
const mongoDB = require('./MongoDB');
const email = require('./Email');
const config = require("./Config").getConfig();
const tor_axios = require('tor-axios');

class Cron {
	static get instance() {
		if (Cron.singleton) return Cron.singleton;
		Cron.singleton = new Cron();
		return Cron.singleton;
	}

	constructor() {
		// Server Control
		this.heartbeartTimer = null;
		this.sleepTimer = 20 // segundos
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;

		this.search_list = [];
		this.tor = null;
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list_falabella');
		this.change_detection = await mongoDB.collection('change_detection_falabella');
		this.products_change_detection = await mongoDB.collection('products_change_detection_falabella');

		this.tor = tor_axios.torSetup({
			ip: 'localhost',
			port: 9050,
			controlPort: '9051',
			controlPassword: 'giraffe',
		});

		this.heartbeatExecute();

	}

	async heartbeatExecute() {
		const validateTimeOut = async () => {
			try {
				this.search_list = await this.search_list_collections.find({ active: true }).toArray();

				for (let i = 0; i < this.search_list.length; i++) {
					let f = this.search_list[i];
					let now = Date.now();
					let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
					let reduce = [];
					let productosNuevos = [];
					let pages = f.pages || 1;
					for (let j = 1; j <= pages; j++) {
						var config = {
							method: 'get',
							url: f.url,
							headers: f.headers
						};

						if (f.discount) f.url += `&facetSelected=true&f.range.derived.variant.discount${f.discount}%25+dcto+y+más`;
						if (f.pages) f.url += `&page=${j}`;
						console.log('[' + today + '][ Falabella - heartbeatExecute] init :', f.url);

						await this.tor.torNewSession()
							.then(async responseip => {
								try {
									const inst = await axios.create({
										httpAgent: this.tor.httpAgent(),
										httpsagent: this.tor.httpsAgent(),
									});

									await inst(config)
										.then(async (response) => {
											let products = JSON.parse(response.data.split('<script id="__NEXT_DATA__" type="application/json">')[1].split('</script>')[0]);

											if (products.props.pageProps.results) console.log('[falabella] Cantidad de productos :', products.props.pageProps.results.length);
											else console.log('No hay productos');

											for (let i = 0; i < products.props.pageProps.results.length; i++) {
												let e = products.props.pageProps.results[i];
												if (e.discountBadge && parseInt(e.discountBadge.label.replace('-', '').replace('%', '')) >= f.discount) {
													let cmrPrice = e.prices.find(e => e.type == "cmrPrice");
													let internetPrice = e.prices.find(e => e.type == "internetPrice");
													let normalPrice = e.prices.find(e => e.type == "normalPrice");
													let eventPrice = e.prices.find(e => e.type == "eventPrice");

													let arrSort = [];
													if (cmrPrice && cmrPrice.price[0]) {
														arrSort.push(cmrPrice.price[0]);
														cmrPrice = cmrPrice.price[0];
													} else cmrPrice = 0;
													if (internetPrice && internetPrice.price[0]) {
														arrSort.push(internetPrice.price[0]);
														internetPrice = internetPrice.price[0];
													} else internetPrice = 0;
													if (eventPrice && eventPrice.price[0]) {
														arrSort.push(eventPrice.price[0]);
														eventPrice = eventPrice.price[0];
													} else eventPrice = 0;

													if (normalPrice) {
														normalPrice = normalPrice.price[0];
													} else normalPrice = 0;
													arrSort.sort(this.compareNumbers);
													reduce.push({
														sku: e.productId,
														searchId: f._id.toString(),
														timestamp: now,
														date: today,
														displayName: e.displayName,
														url: e.url,
														cmrPrice,
														internetPrice,
														normalPrice,
														eventPrice,
														saving: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseInt((normalPrice || internetPrice).replace(/\./, '')) - parseInt(arrSort[0].replace(/\./, ''))),
														discount: e.discountBadge.label,
														image: e.mediaUrls[0]
													})
												}
											};
										})
										.catch((error) => {
											console.log('[falabella] axios error ', f.url, error.response.status);
										});
								} catch (error) {
									console.log('[falabella] error global:', f.url, error);
								};
							});
					};

					for await (let r of reduce) {
						let found = await this.products_change_detection.findOne({ sku: r.sku,  cmrPrice: r.cmrPrice, internetPrice: r.internetPrice, eventPrice: r.eventPrice });
						if (!found) {
							console.log("Product :", r);
							productosNuevos.push(r);
							await this.products_change_detection.insertOne(r);
						};
					};
					if (productosNuevos.length > 0) await this.sendNotification(productosNuevos);
				}
			} catch (e) {
				console.error(e);
			} finally {
				this.heartbeartTimer = setTimeout( validateTimeOut, 5000);
			};
		};
		validateTimeOut();
	};
	

	compareNumbers(a, b) {
		return a - b;
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms * 1000));
	}

	async stop() {
		if (this.heartbeartTimer) {
			console.log(`[CronFalabella][stop] > Eliminando Timer 'heartbeartTimer`);
			clearTimeout(this.heartbeartTimer);
			this.heartbeartTimer = null;
		}
	}

	async sendNotification(productosNuevos) {
		try {
			console.log("[CronFalabella] sendNotification", productosNuevos.length);

			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
						.pdp-mobile-sales-price-cmr {
							font-size: 12px;
							font-weight: 500;
							color: #009933;
						}
						.pdp-mobile-sales-price {
							font-size: 12px;
							font-weight: 500;
							color: #414042;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
			productosNuevos.forEach(data => {
				html +=
					`<div class="row">		
						<div class="card">
							<div style="width: 20%" >
								<img src="${data.image}" alt="Image" style="max-width: 140px">
							</div>
							<div class="product-detail-card__product-prices">
								<div class="d-flex">
								<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 600;">${data.sku} </span> -  ${data.displayName}</a> </h1>
								</div><div class="d-flex pdp-mobile-reference-price">

								<span class="pdp-mobile-sales-price-cmr"> CMR : $${data.cmrPrice} </span>
								</div><div class="d-flex pdp-mobile-reference-price">

								<span class="pdp-mobile-sales-price">Internet : $${data.internetPrice} </span>
								</div><div class="d-flex pdp-mobile-reference-price">

								<span class="pdp-mobile-sales-price">Precio : $${data.eventPrice} </span>
								<span class="pdp-mobile-discount-percentage-card">${data.discount}</span>

								</div><div class="d-flex pdp-mobile-reference-price">
								<span class="saving__price__pdp"><del>${data.normalPrice}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
			});
			html += `</div> </body> </html>`;

			email.sendNotification(html, 'falabella')
				.catch(error => {
					console.log('[Email][sendNotification] Send email error >', error);
					setTimeout(() => {
						email.sendNotification(html, 'falabella');
					}, 5000);
				});
		} catch (err) {
			console.log('[CronFalabella][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = Cron.instance;
