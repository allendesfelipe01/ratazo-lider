var axios = require('axios');
const mongoDB = require('./MongoDB');
const nodemailer = require('nodemailer');
const config = require("./Config").getConfig();
const tor_axios = require('tor-axios');

class CronAdidas {
	static get instance() {
		if (CronAdidas.singleton) return CronAdidas.singleton;
		CronAdidas.singleton = new CronAdidas();
		return CronAdidas.singleton;
	}

	constructor() {
		// Server Control
		this.heartbeartTimer = null;
		this.heartbeatInterval = 10; //minutos
		this.sleepTimer = 20 // segundos		
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;
		this.contacts_collection = null;

		this.search_list = [];
		this.transporter = null;
		this.notify = null;
		this.tor = null;
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list_adidas');
		this.change_detection = await mongoDB.collection('change_detection_adidas');
		this.products_change_detection = await mongoDB.collection('products_change_detection_adidas');
		this.contacts_collection = await mongoDB.collection('contacts');


		this.notify = config.notify;

		const cfgMail = {
			host: this.notify.email.host,
			port: this.notify.email.port,
			secure: this.notify.email.secure,
			from: this.notify.email.from,
			auth: {
				user: this.notify.email.user,
				pass: this.notify.email.pswd,
			}
		};

		this.transporter = nodemailer.createTransport(cfgMail);
		await this.transporter.verify().then(e => console.log('conectado'));

		this.tor = tor_axios.torSetup({
			ip: 'localhost',
			port: 9050,
			controlPort: '9051',
			controlPassword: 'giraffe',
		});

		this.callHeartbeatExecute(true);

	}

	callHeartbeatExecute(first) {
		try {
			this.heartbeartTimer = setTimeout(async _ => {
				let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
				console.log('[' + today + '][CronAdidas][callHeartbeatExecute] init');
				this.heartbeartTimer = null;
				await this.heartbeatExecute();
			}, first ? 0 : this.heartbeatInterval*1000*60);
		} catch (e) {
			console.error(e);
			throw e;
		}
	}

	async heartbeatExecute() {
		try {
			this.search_list = await this.search_list_collections.find({ active: true }).toArray();

			for (let i = 0; i < this.search_list.length; i++) {
				let f = this.search_list[i];
				let now = Date.now();
				let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
				let reduce = [];

				let print = false;
				let productosNuevos = [];
				let pages = f.pages || 1;

				for (let j = 1; j <= pages; j++) {
					let url = f.url;

					var config = {
						method: 'get',
						url: f.url,
						headers: f.headers,
						timeout: 600000
					};
				
					if(f.pages) url += `&page=${j}&s=mdco`;
					config.url = url;

					console.log('[' + today + '][ Adidas - heartbeatExecute] init :', config.url);
					
					await this.tor.torNewSession()
					.then( async responseip => {
						//change tor ip
						console.log(responseip);
						//await this.sleep(this.sleepTimer);
						try{

							let response_newip = await this.tor.get('http://api.ipify.org');
							console.log('[adidas] ', response_newip.data);

							const inst = await axios.create({
								httpAgent: this.tor.httpAgent(),
								httpsagent: this.tor.httpsAgent(),
							});
	
							await inst(config)
								.then(async (response) => {
									console.log("response", response.data );									
								})
								.catch((error) => {
									console.log('[adidas] axios error ', config.url, error);
								});
						}catch(error){
							console.log('[adidas] error global:', config.url,  error);
						};
					});
					if (productosNuevos.length > 0) await this.sleep(this.sleepTimer);
				};
			}
		} catch (e) {
			console.error(e);
			throw e;
		} finally {
			this.callHeartbeatExecute(false);
		}
	}

	compareNumbers(a, b) {
		return a - b;
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms*1000));
	}

	async stop() {
		if (this.heartbeartTimer) {
			console.log(`[CronAdidas][stop] > Eliminando Timer 'heartbeartTimer`);
			clearTimeout(this.heartbeartTimer);
			this.heartbeartTimer = null;
		}
	}

	async sendNotification(productosNuevos) {
		try {
			console.log("[CronAdidas] sendNotification", productosNuevos);


			let subject = this.notify.email.subject_adidas;


			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
						.pdp-mobile-sales-price-adidas {
							font-size: 14px;
							font-weight: 500;
						}
						.pdp-mobile-sales-price {
							font-size: 14px;
							font-weight: 500;
							color: #414042;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
				productosNuevos.forEach(data => {
					html += 	
					`<div class="row">		
						<div class="card">
							<div style="max-width: 20%" >
								<img src="${data.image}" alt="Image" style="max-width: 100px">
							</div>
							<div class="product-detail-card__product-prices" style="max-width: 80%">
								<div class="d-flex">
									<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 600;">${data.sku} </span> -  ${ data.displayName }</a> </h1>
								</div>

								<div class="d-flex">
									<span class="pdp-mobile-sales-price">Internet : ${data.offerPrice} </span>
									<span class="pdp-mobile-discount-percentage-card">-${data.discount}%</span>
								</div><div class="d-flex pdp-mobile-reference-price">
								<span class="saving__price__pdp"><del>${data.listPrice}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
				});
			html += `</div> </body> </html>`;
			
			let contacts = await this.contacts_collection.find({  active: true }).toArray();
				
			for(let contact of contacts){
				let mailOption = {
					from: this.notify.email.from,
					to: contact.email,
					subject,
					html
				}
				this.transporter.sendMail(mailOption, function (err, info) {
					if (err) {
						console.log('[CronAdidas][sendNotification] Send email error >', err);
						return false;
					} else {
						console.log('[CronAdidas][sendNotification] > Send OK');
						return true;
					}
				});
			}
		} catch (err) {
			console.log('[CronAdidas][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = CronAdidas.instance;
