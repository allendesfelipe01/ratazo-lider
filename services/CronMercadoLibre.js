var axios = require('axios');
const mongoDB = require('./MongoDB');
const email = require('./Email');
const config = require("./Config").getConfig();
const tor_axios = require('tor-axios');

class CronMercadoLibre {
	static get instance() {
		if (CronMercadoLibre.singleton) return CronMercadoLibre.singleton;
		CronMercadoLibre.singleton = new CronMercadoLibre();
		return CronMercadoLibre.singleton;
	}

	constructor() {
		// Server Control
		this.heartbeartTimer = null;
		this.heartbeatInterval = 10;// minutos
		this.sleepTimer = 20; // segundos
		this.search_collections = null;
		this.change_detection = null;
		this.products_change_detection = null;

		this.search_list = [];
		this.tor = null;
	};

	async init() {
		this.search_list_collections = await mongoDB.collection('search_list_mercado_libre');
		this.change_detection = await mongoDB.collection('change_detection_mercado_libre');
		this.products_change_detection = await mongoDB.collection('products_change_detection_mercado_libre');

		this.tor = tor_axios.torSetup({
			ip: 'localhost',
			port: 9050,
			controlPort: '9051',
			controlPassword: 'giraffe',
		});

		this.heartbeatExecute();
	};

	async heartbeatExecute() {
		const validateTimeOut = async () => {
			try {
				this.search_list = await this.search_list_collections.find({ active: true }).toArray();

				for (let i = 0; i < this.search_list.length; i++) {
					let f = this.search_list[i];
					let now = Date.now();
					let today = new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
					let reduce = [];

					let productosNuevos = [];
					let pages = f.pages || 1;

					for (let j = 1; j <= pages; j++) {
						let url = f.url;

						var config = {
							method: 'get',
							url: f.url,
							headers: f.headers,
							timeout: 5000
						};

						if (f.pages) url += `&page=${j}`;
						config.url = url;

						console.log('[' + today + '][ Mercado Libre - heartbeatExecute] init :', config.url);

						await this.tor.torNewSession()
							.then(async responseip => {
								try {
									const inst = await axios.create({
										httpAgent: this.tor.httpAgent(),
										httpsagent: this.tor.httpsAgent(),
									});

									await inst(config)
										.then(async (response) => {

											if (f.type == 'list') {
												let response_mercado_libre = JSON.parse(response.data.split('window.__PRELOADED_STATE__ =')[1].split('}},{s:"https://http2.mlstatic.com/frontend-assets/search-nordic/search.desktop.1d91b450.js')[0].replace(/\\u002F/g, '/').slice(0, -10));
												if (response_mercado_libre.initialState.results) console.log('[mercado libre] Cantidad de productos :', response_mercado_libre.initialState.results.length);
												else console.log('No hay productos');

												productosNuevos = [];

												for (let i = 0; i < response_mercado_libre.initialState.results.length; i++) {
													let e = response_mercado_libre.initialState.results[i];

													if (e.price && e.price.discount_rate >= f.discount && e.available_quantity > 0) {

														reduce.push({
															sku: e.id,
															searchId: f._id.toString(),
															timestamp: now,
															date: today,
															displayName: e.title,
															url: e.permalink,
															price: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(e.price.amount),
															saving: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(e.price.original_price - e.price.amount),
															original_price: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(e.price.original_price),
															discount: e.price.discount_rate,
															image: e.pictures.stack.retina
														})
													}
												};
											} else if (f.type == 'category') {

												let response_mercado_libre = JSON.parse(response.data.split('window.__PRELOADED_STATE__ =')[1].split('window.__CUSTOM_STATE__')[0].replace(/\\u002F/g, '/').slice(0, -12));
												let products = response_mercado_libre.dataLanding.components.find(e => e.component_name == 'PaginationListing');
												if (products) {
													if (products.data.items.length > 0) console.log('[mercado libre] Cantidad de productos :', products.data.items.length);
													else console.log('No hay productos');
												}

												productosNuevos = [];

												for (let i = 0; i < products.data.items.length; i++) {
													let e = products.data.items[i];

													if (e.price && e.price.discount >= f.discount && e.installments.quantity > 0) {
														reduce.push({
															sku: e.id,
															searchId: f._id.toString(),
															timestamp: now,
															date: today,
															displayName: e.title,
															url: e.link.url,
															price: e.price.price,
															saving: new Intl.NumberFormat('es-CL', { currency: 'CLP', style: 'currency' }).format(parseInt(e.price.original_price.replace(/\./g, '')) - parseInt(e.price.price.replace(/\./g, ''))),
															original_price: e.price.original_price,
															discount: e.price.discount,
															image: e.image_src
														})
													}
												};
											};
										})
										.catch((error) => {
											console.log('[mercado libre] axios error ', config.url, error);
										});
								} catch (error) {
									console.log('[mercado libre] error global:', config.url, error);
								};
							});
					};

					for await (let r of reduce) {
						let found = await this.products_change_detection.findOne({ sku: r.sku, price : r.price });
						if (!found) {
							console.log("Product New :", r);
							productosNuevos.push(r);
							await this.products_change_detection.insertOne(r);
						};
					};
					if (productosNuevos.length > 0) await this.sendNotification(productosNuevos);
				}
			} catch (e) {
				console.error(e);
			} finally {
				this.heartbeartTimer = setTimeout( this.heartbeatExecute, 5000);
			};
		};
		validateTimeOut();
	};

	compareNumbers(a, b) {
		return a - b;
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms * 1000));
	}

	async stop() {
		if (this.heartbeartTimer) {
			console.log(`[CronMercadoLibre][stop] > Eliminando Timer 'heartbeartTimer`);
			clearTimeout(this.heartbeartTimer);
			this.heartbeartTimer = null;
		}
	}

	async sendNotification(productosNuevos) {
		try {
			console.log("[CronMercadoLibre] sendNotification", productosNuevos.length);
			let html = `<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					<style>
						.card {
							display: flex;
							box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
							max-width: 100%;
							margin: auto;
							font-family: arial;
							justify-content: flex-start;
							font-family: "Gill Sans", sans-serif;
						}
						
						.title {
							color: grey;
							font-size: 18px;
						}
						
						a {
							text-decoration: none;
							font-size: 22px;
							color: black;
						}
			
						.descuento {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							margin-top: 1px;
							margin-left: 6px;
							padding: 4px;
						}
						.precio {
							width: 35px;
							border-radius: 16px;
							font-size: 20px;
							font-weight: 300;
							text-align: center;
							color: #000;
							margin-top: 1px;
							margin-left: 6px;
						}
			
						.product-detail-card__product-prices {
							padding: 20px 0;
						}   
						.pdp-mobile-sales-price-mercado-libre {
							font-size: 14px;
							font-weight: 500;
						}
						.pdp-mobile-sales-price {
							font-size: 16px;
							font-weight: 500;
							color: #414042;
						}
			
						.pdp-mobile-discount-percentage-card {
							width: 35px;
							border-radius: 16px;
							background-color: #de1c24;
							font-size: 18px;
							font-weight: 700;
							text-align: center;
							color: #fff;
							height: 20px;
							margin-top: 10px;
							margin-left: 10px;
							padding: 3px;
						}
			
						.pdp-desktop-pay {
							font-size: 12px;
							font-weight: 400;
							font-style: italic;
							font-stretch: normal;
							line-height: 1.22;
							letter-spacing: normal;
							color: #6d6e71;
							margin-top: 15px;
							margin-left: 5px;
						}
			
						.pdp-mobile-reference-price {
							font-size: 14px;
							color: #414042;
						}
			
						.product-detail-display-name {
							font-size: 20px;
							color: #414042;
							display: flex;
							font-weight: 400;
							line-height: 1.5;
							margin-bottom: 0;
							padding-right: 20px;
						}
					</style>
				</head>
			<body>	
				<div class="column" style="width:100%">`;
			productosNuevos.forEach(data => {
				html +=
					`<div class="row">		
						<div class="card">
							<div style="max-width: 20%" >
								<img src="${data.image}" alt="Image" style="max-width: 100px">
							</div>
							<div class="product-detail-card__product-prices" style="max-width: 80%">
								<div class="d-flex">
									<h1 class="product-detail-display-name"><a href="${data.url}"> <span style="font-weight: 600;">${data.sku} </span> -  ${data.displayName}</a> </h1>
								</div>

								<div class="d-flex">
									<span class="pdp-mobile-sales-price">Precio : ${data.price} </span>
									<span class="pdp-mobile-discount-percentage-card">-${data.discount}%</span>
								</div><div class="d-flex pdp-mobile-reference-price">
								<span class="saving__price__pdp"><del>${data.original_price}</del></span> | Ahorro: <span>${data.saving}</span>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>`
			});
			html += `</div> </body> </html>`;

			email.sendNotification(html, 'mercado_libre')
				.catch(error => {
					console.log('[Email][sendNotification] Send email error >', error);
					setTimeout(() => {
						email.sendNotification(html, 'mercado_libre');
					}, 5000);
				});

		} catch (err) {
			console.log('[CronMercadoLibre][sendNotification] Execute error >', err.toString());
			return false;
		}
	}
}

module.exports = CronMercadoLibre.instance;
