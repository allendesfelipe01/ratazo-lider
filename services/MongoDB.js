const config = require("./Config").getConfig();
const MongoClient = require('mongodb').MongoClient;
const TimeUtils = require("./TimeUtils");

class MongoDB {
    constructor() {
        this.client = null;
        this.db = null;
    }
    static get instance() {
        if (!MongoDB.singleton) MongoDB.singleton = new MongoDB();
        return MongoDB.singleton;
    }

    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    async init() {
        return new Promise(async (resolve) => {
            let mongoURL = config.mongoDB.url;
            try {
                if (process.env.MONGO_URL) {
                    mongoURL = process.env.MONGO_URL;
                    console.log("[Ratazo][MongoDB] Using URL from environment configuration");
                } else {
                    console.log("[Ratazo][MongoDB] Using URL from local configuration");
                }
                this.client = new MongoClient(mongoURL, {useNewUrlParser:true,useUnifiedTopology: true });
                await this.client.connect();
                let mongoDatabase = config.mongoDB.database;
                if (process.env.MONGO_DATABASE) {
                    mongoDatabase = process.env.MONGO_DATABASE;
                    console.log("[Ratazo][MongoDB] Using Database from environment configuration");
                } else {
                    console.log("[Ratazo][MongoDB] Using Database from local configuration");
                };

                this.db = this.client.db(mongoDatabase);
                resolve(this.db);
            } catch(error) {
                console.error("[Ratazo][MongoDB] Cannot connecto to Database. Using URL:" + mongoURL);
                console.error(error);
                process.exit(1);
            };
        })
    };

    collection(name) {
        return new Promise((resolve, reject) => {
            this.db.collection(name, (err, col) => {
                if (err) reject(err);
                else resolve(col);
            });
        });
    }
}

module.exports = MongoDB.instance;