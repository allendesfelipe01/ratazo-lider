const TimeUtils = require("./TimeUtils");
const mongoDB = require('./MongoDB');
const nodemailer = require('nodemailer');

class Email {
    constructor() {
        this.emails = null;
        this.currentEmail = null;
        
        this.transporter = null;
		this.notify = null;

        this.email_collection = null;
        this.contacts_collection;
    }
    static get instance() {
        if (!Email.singleton) Email.singleton = new Email();
        return Email.singleton;
    }

    async init() {
        console.log('[init] Email Service');
        return new Promise(async (resolve) => {
            try {
                this.email_collection = await mongoDB.collection('emails');
                this.contacts_collection = await mongoDB.collection('contacts');

                this.emails = await this.email_collection.find().toArray();
                console.log('[init] Email Service', this.emails);

                if(this.emails.length > 0){
                    this.currentEmail = this.emails[0];
                } else console.error('No se pudo conectar al correo');
                this.connect()
                .then(r => {console.log(r); resolve(); })
                .catch(error => {
                    console.error(error);                 
                    process.exit(1);
                });

            } catch(error) {
                console.error(error);
            };
        })
    };

    async connect(){
        console.log("[email] Try Connect ...");
        return new Promise (async (resolve, reject) => {
            const cfgMail = {
                host: this.currentEmail.host,
                port: this.currentEmail.port,
                secure: this.currentEmail.secure,
                from: this.currentEmail.from,
                auth: {
                    user: this.currentEmail.user,
                    pass: this.currentEmail.pswd,
                }
            };

            this.transporter = nodemailer.createTransport(cfgMail);
            await this.transporter.verify().then(e => resolve('Conectado correctamente al correo.')).catch(error => reject(error));
        });
    };

    async sendNotification(html, app) {
        return new Promise(async (resolve, reject) => {

		try {
			console.log('[Ratazo]['+ app+ '][Try Send Notification]');

			let contacts = await this.contacts_collection.find({  active: true }).toArray();
				
			for(let contact of contacts){
				let mailOption = {
					from: this.currentEmail.from,
					to: contact.email,
					subject : this.currentEmail['subject_' + app],
					html
				};

				this.transporter.sendMail(mailOption, (err, info) => {
					if (err) {
						console.log('[Ratazo]['+ app+ '][sendNotification] Send email error >', err);
                        let emails_distincts = this.emails.filter(e => JSON.stringify(e) != JSON.stringify(this.currentEmail));
                        let rand = Math.floor(Math.random() * emails_distincts.length); ;
                        this.currentEmail = emails_distincts[rand];
                        this.connect()
                        .then(r => console.log(r) )
                        .catch(error => {
                            console.error(error);                 
                        });
                        reject(err);
					} else {
						console.log('[Ratazo]['+ app+ '][sendNotification] > Send OK');
						resolve('Envío exitoso.')
					};
				});
			}
		} catch (err) {
			console.log('[Email]['+ app+ '][sendNotification] Execute error >', err.toString());
			return false;
		}
        })
	}
}

module.exports = Email.instance;